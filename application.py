import logging
from datetime import timedelta
from datetime import date
from pathlib import Path
import yaml
from os import walk

from paperfeeds import logger
from paperfeeds.feeds import process_urls, get_rss_urls, get_nbib_urls, get_ris_urls, get_csv_urls

log = logging.getLogger(__name__)


def load_config(config_path='config.yml'):
    configuration = dict()
    with open(config_path) as file:
        configuration = yaml.full_load(file)
    return configuration


if __name__ == '__main__':
    urls = []
    config = load_config()
    settings = config.get('settings') or dict()
    authentication = config.get('authentication') or dict()
    sources = config.get('sources') or dict()
    start_date = sources.get('start_date') or str(date.today() - timedelta(days=1))
    end_date = sources.get('end_date') or str(date.today())

    api_key = authentication.get('api_key') or ''
    endpoint = settings.get('endpoint')
    readership_level = settings.get('readership_level') or 'technical-readership-accurate'
    summary_type = settings.get('summary_type') or 'combined'
    structured_summary = settings.get('structured_summary') or False
    format_summary = settings.get('format_summary') or False
    headline_type = settings.get('headline_type')
    wiki_links = settings.get('wiki_links') or False
    output_format = settings.get('output_format') or 'text/html'
    output_directory = sources.get('output_directory') or str(Path.home())
    input_directory = sources.get('input_directory')
    server = sources.get('server')
    categories = sources.get('categories') or []
    rss_urls = sources.get('rss_urls')
    ris_path = sources.get('ris_path')
    nbib_path = sources.get('nbib_path')
    csv_path = sources.get('csv_path')
    max_results = sources.get('max_results', None)
    map_urls = settings.get('map_urls') or False

    if rss_urls:
        input_directory = None
        urls = get_rss_urls(rss_urls, map_urls=map_urls)
    elif nbib_path:
        input_directory = None
        urls = get_nbib_urls(nbib_path)
    elif ris_path:
        input_directory = None
        urls = get_ris_urls(ris_path)
    elif csv_path:
        input_directory = None
        urls = get_csv_urls(csv_path)
    elif input_directory:
        urls = next(walk(input_directory), (None, None, []))[2]

    # Truncate list of urls/files
    log.info("Truncating to: {}".format(max_results))
    urls = urls[:max_results]
    log.info(urls)

    process_urls(endpoint=endpoint,
                 url_list=urls,
                 input_directory=input_directory,
                 readership_level=readership_level,
                 summary_type=summary_type,
                 structured_summary=structured_summary,
                 format_summary=format_summary,
                 headline_type=headline_type,
                 auth_token=api_key,
                 output_directory=output_directory,
                 wiki_links=wiki_links,
                 output_format=output_format
                 )

