from paperfeeds.parser import parse_nbib, parse_ris, parse_bibtex

import requests
import feedparser
from pathlib import Path
from datetime import timedelta
from datetime import date
import os
import csv
import re
import logging

log = logging.getLogger(__name__)


BIORXIV_ENDPOINT = "https://api.biorxiv.org/details/{}/"
SUMMARIZER_ENDPOINT = "https://summarizer.scholarcy.com/summarize"
DOI_ENDPOINT = "https://doi.org"

LICENSE_TYPES = {'cc_by', 'cc_0', 'cc_by_sa', 'cc_by_nc', 'cc_by_nd', 'cc_no'}
# LICENSE_TYPES = {'cc_by', 'cc_0'}

RSS_MAPPINGS = [
    {'s': re.compile(r'https://preprints[.]jmir[.]org/preprint/(\d+)'),
     'r': r'https://s3.ca-central-1.amazonaws.com/assets.jmir.org/assets/preprints/preprint-\1-submitted.pdf'},
    {'s': re.compile(r'/rs-(\d+)/latest'), 'r': r'/rs-\1/latest.pdf'},
    {'s': re.compile(r'/abs/'), 'r': '/pdf/'},
    {'s': re.compile(r'[?]rss=1"?$'), 'r': '.pdf'},
]


TEMPLATE_BODY = """
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="https://www.scholarcy.com/wp-content/uploads/2018/10/scholarcy-favicon.png" type="image/x-icon" />
    <script src="https://summarizer.scholarcy.com/static/js/jquery-3.4.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://summarizer.scholarcy.com/static/js/bootstrap.min.js"></script>
    <script src="https://summarizer.scholarcy.com/static/js/functions.js"></script>

    <link href="https://summarizer.scholarcy.com/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://summarizer.scholarcy.com/static/css/app.css" rel="stylesheet">

    <title>Draft review</title>
    <style>
        .reference-item {{
            padding-left: 1em;
            text-indent: -1em;
            margin-bottom: 0.5em;
        }}
    </style>
</head>
<body>
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-10">
        <h4 id="synopsis-head">{0}</h4>
"""

TEMPLATE_REFS = """
<h4 id="references-head">References</h4>
<div id="reference-list">
{0}
</div>
"""

TEMPLATE_TAIL = """
      </div>
    </div>
  </div>
  <script>
        $('body').tooltip({
          selector: '.has-tooltip'
        });
  </script>
</body>
</html>
"""


def get_csv_urls(file_path):
    urls = []
    with open(file_path) as csv_file:
        c = csv.reader(csv_file)
        for row in c:
            for i in row:
                if re.match(r'https?:', i):
                    urls.append(i)
                elif re.match(r'10[.]\d+/', i):
                    urls.append("{}/{}".format(DOI_ENDPOINT, i))
    return urls


def get_nbib_urls(file_path):
    records = parse_nbib(file_path)
    return ["{}/{}".format(DOI_ENDPOINT, doi) for r in records if r.get('DO') for doi in r['DO']]


def get_ris_urls(file_path):
    records = parse_ris(file_path)
    return ["{}/{}".format(DOI_ENDPOINT, doi) for r in records if r.get('DO') for doi in r['DO']]


def map_url(url=None):
    if not url:
        return url
    mapped_url = url
    # Perform any domain/url mapping, e.g. for arXiv.org
    for item in RSS_MAPPINGS:
        try:
            mapped_url = item.get('s').sub(item.get('r'), mapped_url)
        except Exception as e:
            pass
    return mapped_url


def get_rss_urls(rss_url_list=None, map_urls=True):
    if not rss_url_list:
        rss_url_list = []
    document_links = []
    links = []
    for rss_url in rss_url_list:
        links = []
        try:
            parsed = feedparser.parse(rss_url)
            links = [e.link for e in parsed.entries]
        except Exception as e:
            log.error(e)
        document_links += [map_url(l) for l in links]
    return document_links if map_urls else links


def get_biorxiv_urls(server='biorxiv',
                     categories=None,
                     start_date=str(date.today() - timedelta(days=1)),
                     end_date=str(date.today())
                     ):
    if not categories:
        categories = []
    endpoint = BIORXIV_ENDPOINT.format(server)
    url = "{}{}/{}".format(endpoint, start_date, end_date)
    # TODO: this needs work - only the first 100 results are returned, need to paginate to get the rest
    r = requests.get(url, timeout=60)
    data = r.json()
    if categories:
        urls = ["https://doi.org/{}".format(i.get('doi')) for i in data.get('collection') if
                i.get('license') in LICENSE_TYPES and i.get('category') in categories]
    else:
        urls = ["https://doi.org/{}".format(i.get('doi')) for i in data.get('collection') if
            i.get('license') in LICENSE_TYPES]
    return urls


def process_files(file_paths=None,
                  output_directory=str(Path.home()),
                  readership_level='technical-readership-accurate',
                  structured_summary=False,
                  summary_type='combined',
                  auth_token=''
                  ):
    payload = {
        'wiki_links': True,
        'format_summary': True,
        'readership_level': readership_level,
        'structured_summary': structured_summary,
        'summary_type': summary_type,
        'TOKEN': auth_token
    }
    headers = {
        'Authorization': 'Bearer ' + auth_token,
        'Accept': 'text/html'
    }
    variant = 'v2' if structured_summary else 'v1'
    if not file_paths:
        file_paths = []
    for idx, file_path in enumerate(file_paths):
        output_filename = os.path.basename(file_path)
        log.info('Processing {} ...'.format(file_path))
        with open(file_path, 'rb') as f:
            files = {'file': f}
            r = requests.post(SUMMARIZER_ENDPOINT, data=payload, headers=headers, files=files)
            with open("{}/{}-{}-{}-{}-{:03d}.html".format(output_directory, output_filename, readership_level, summary_type, variant, idx), 'w') as f:
                f.write(r.text)


def process_urls(endpoint=SUMMARIZER_ENDPOINT,
                 url_list=None,
                 input_directory=None,
                 output_directory=str(Path.home()),
                 readership_level='technical-readership-accurate',
                 structured_summary=False,
                 summary_type='combined',
                 format_summary=True,
                 headline_type=None,
                 wiki_links=True,
                 output_format='text/html',
                 auth_token=''):
    payload = {
        'wiki_links': wiki_links,
        'format_summary': format_summary,
        'headline_type': headline_type,
        'readership_level': readership_level,
        'structured_summary': structured_summary,
        'summary_type': summary_type,
        'TOKEN': auth_token
    }
    headers = {
        'Authorization': 'Bearer ' + auth_token,
        'Accept': output_format
    }
    variant = 'v2' if structured_summary else 'v1'
    synopsis_filename = "{}-{}-{}-{}".format('review' if format_summary else 'synopsis', readership_level, summary_type, variant)
    synopsis_heading = 'Review' if format_summary else 'Synopsis'

    if not url_list:
        url_list = []
    reference_list = []
    extension = 'html' if output_format == 'text/html' else 'json'
    with open("{}/{}.{}".format(output_directory, synopsis_filename, extension), 'w') as review_file:
        review_file.write(TEMPLATE_BODY.format(synopsis_heading))
        for idx, url in enumerate(url_list):
            output_filename = os.path.basename(url).split('?')[0]
            # Skip hidden files
            if url.startswith('.'):
                continue
            log.info('Processing: {} ...'.format(url))
            if input_directory:
                files = {'file': open("{}/{}".format(input_directory, url), 'rb')}
                r = requests.post(endpoint, data=payload, files=files, headers=headers)
            else:
                payload['url'] = url
                r = requests.get(endpoint, params=payload, headers=headers)
            content = r.text
            with open("{}/{}-{}-{}-{}-{:03d}.{}".format(output_directory, output_filename, readership_level, summary_type, variant, idx, extension), 'w') as f:
                f.write(content)
            blockquote = re.search(r'<blockquote id="headline">(.+?)</blockquote>', content, flags=re.M + re.DOTALL)
            references = re.search(r'<div id="reference-list">(.+?)</div>', content, flags=re.M + re.DOTALL)
            if blockquote:
                # Remove any trailing breaks
                synopsis = re.sub(r'<br/><br/>(\s+</p>)', r'\1', blockquote.group(1))
                # Remove Google Translate
                synopsis = re.sub(r'</div>.+$', r'</div>', synopsis, flags=re.M + re.DOTALL)
                review_file.write(synopsis)
                if references:
                    reference_list.append('<div class="reference-item">{}</div>'.format(references.group(1)))

        if format_summary:
            review_file.write(TEMPLATE_REFS.format('\n'.join(reference_list)))
        review_file.write(TEMPLATE_TAIL)

