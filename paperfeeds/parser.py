import regex
from collections import defaultdict
import bibtexparser

import logging


log = logging.getLogger(__name__)

INDENT_L2 = '\n                '
INDENT_L1 = '\n            '

RIS_PATTERN = regex.compile("^([A-Z][A-Z0-9]) *- *(.*)")
NBIB_PATTERN = regex.compile("^([A-Z]{2,4}) *- *(.*)")
RIS_XML_MAPPING = {
    'TY': [None],
    'AU': ['author'],
    'TI': ['article_title'],
    'T1': ['article_title'],
    'T2': ['journal_title', 'volume_title', 'series_title'],
    'JO': ['journal_title'],
    'JF': ['journal_title'],
    'ET': ['edition_number'],
    'VL': ['volume'],
    'SP': ['first_page'],
    'IS': ['issue'],
    'PY': ['cYear'],
    'Y1': ['cYear'],
    'DO': ['doi']
}


def extract_doi(text):
    m = regex.search(r'\b(10[.]\d{4,9}/[^\s]+)', text)
    if m:
        return m.group()
    return None


def file_to_text(file_path):
    doc_text = ''
    with open(file_path, 'rb') as f:
        try:
            doc_text = f.read().decode('utf-8-sig')
        except UnicodeDecodeError:
            try:
                doc_text = f.read().decode('utf-8')
            except UnicodeDecodeError:
                try:
                    doc_text = f.read().decode('latin-1')
                except UnicodeDecodeError as e:
                    log.error("{}: Text encoding problem with {}".format(e, file_path))
    return doc_text


def parse_bibtex(file_path):
    bibtex = []
    try:
        with open(file_path) as bibtex_file:
            bibtex_database = bibtexparser.load(bibtex_file)
            bibtex = bibtex_database.entries
    except Exception as e:
        log.error(e)
    return bibtex


def parse_nbib(file_path):
    """
    Parses PubMed NBIB data and converts to RIS
    :param file_path: input NBIB file
    :return: RIS records
    """
    current_record = defaultdict(list)
    records = []

    text = file_to_text(file_path)

    if not text:
        return records

    # NBIB records can be multi-line - continuation lines end in [space][line-break]
    clean_text = regex.sub(r'[ ]+[\r\n]*|[\r\n]+[ ]+', ' ', text)
    lines = regex.split(r'[\r\n]+', clean_text)
    for line in lines:
        match = NBIB_PATTERN.match(line)
        tag, field = (match.groups()[0], match.groups()[1]) if match else (None, None)

        # new record field
        if tag == 'PMID':
            current_record = defaultdict(list)
        # end record field
        elif tag == "SO":
            records.append(current_record)
            current_record = defaultdict(list)
        else:
            current_record[tag].append(field)
            # Attempt some tag munging
            if tag == 'DP' and not current_record.get('PY'):
                current_record['PY'].append(field.split()[0])
            if tag in ['A2', 'C1'] and not current_record.get('AU'):
                current_record['AU'].append(field)
            # validate DOI field - delete if not valid
            elif tag in ['LID', 'AID']:
                doi = extract_doi(field)
                if doi:
                    current_record['DO'] = [doi]
                else:
                    # Remove it
                    try:
                        current_record[tag].remove(field)
                    except ValueError:
                        pass
    return records


def parse_ris(file_path):
    """
    Parses RIS data
    :param file_path: input RIS file path
    :return: RIS records
    """
    current_record = defaultdict(list)
    records = []

    text = file_to_text(file_path)

    if not text:
        return records

    # remove blank lines
    lines = [line.strip() for line in filter(lambda x: x.strip(), text.split('\n'))]

    for line in lines:
        match = RIS_PATTERN.match(line)
        if match:
            tag = match.groups()[0]
            field = match.groups()[1]
            """ Process RIS file field """
            if tag == "TY":
                current_record = defaultdict(list)
                current_record[tag] = [field]
            elif tag == "ER":
                records.append(current_record)
                current_record = defaultdict(list)
            else:
                current_record[tag].append(field)
                # Attempt some tag munging
                if tag in ['A2', 'C1'] and not current_record.get('AU'):
                    current_record['AU'].append(field)
                # validate DOI field - delete if not valid
                elif tag == 'DO':
                    doi = extract_doi(field)
                    if doi:
                        current_record['DO'] = [doi]
                    else:
                        current_record['DO'] = []
                elif tag == 'UR':
                    doi = extract_doi(field)
                    if doi:
                        current_record['DO'].append(doi)
        else:
            log.error("Unexpected RIS line format: {}".format(line))
    return records





