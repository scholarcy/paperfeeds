## Version 0.2.0

# What is this
A command line tool to process RSS feeds, RIS/Nbib files, CSV lists of URLs/DOIs, or an input directory
containing PDF, Word, HTML or plain text files, through the Scholarcy Smart Synopsis API endpoint at

    https://summarizer.scholarcy.com/summarize

Documentation on the API is at: https://scholarcy.github.io/slate/#generate-a-synopsis

## Requirements

- Python 3.6.x or higher

## Installation

    pip install -r requirements.txt

## Configuration

1. Create a `config.yml` file in this project directory. Use the `config.example.yml` as a template.
2. Set up an input source. This can be one of:
   - One or more RSS feed URLs
   - A PubMed `.nbib` file
   - A RIS file
   - A CSV file. This should contain at least one column containing either DOIs or public web URLs
   - An input directory containing PDF, Word, HTML or plain text documents
3. If using RSS as input, enter one or more RSS feed urls under the `rss_urls:` section
4. If using PubMed as input, add the path to a PubMed .nbib file to the `nbib_path:` field
5. If using RIS as input, add the path to a RIS file to the `ris_path:` field
6. If using CSV as input, add the path to a CSV file to the `csv_path:` field
7. If using an input directory of documents, add the full path to the input directory to the `input_directory:` field
8. Enter the output directory where the synopses should be stored to the `output_directory:` field
9. Enter your API key to the `api_key:` field

## Running

Make sure you are in the root directory and the in the Terminal run this command:

    python application.py

The chosen input source will be processed and the output written to your `output_directory` location.
Individual synopses will be joined into a single file called 
either `review.html` (if `format_summary` was set to `True`) or 
`synopsis.html` (if `format_summary` was set to `False`)
in your `config.yml` file
